### Terms of Service

The following terms and conditions ("Terms") govern all use of the Kibbut.com website and the Kibbut App on the iOS Apple App Store, (taken together, our "Services"). Our Services are offered subject to your acceptance without modification of all of the terms and conditions contained herein and all other operating rules, policies (including, without limitation, Kibbut's Privacy Policy) and procedures that may be published from time to time by Kibbut LLC (collectively, the "Agreement"). You agree that we may automatically upgrade our Services, and these Terms will apply to any upgrades.

We use the term “Designated Countries” to refer to Australia, Canada, Japan, Mexico, New Zealand, and all countries located in the European continent.

We refer to Kibbut LLC collectively as “Kibbut” or “we” throughout this agreement.

Please read this Agreement carefully before accessing or using our Services. By accessing or using any part of our Services, you agree to become bound by the Terms of this Agreement. If you do not agree to all the Terms of this Agreement, then you may not access or use any of our Services. If these Terms are considered an offer by Kibbut, acceptance is expressly limited to these Terms.

Our Services are not directed to children. Access to and use of our Services is only for those over the age of 13 (or 16 in the European Union). If you are younger than this, you may not register for or use our Services. Any person who registers as a user or provides their personal information to our Services represents that they are 13 years of age or older (16 years or older in the European Union).

Use of our Services requires a Kibbut account. You agree to provide us with complete and accurate information when you register for an account. You will be solely responsible and liable for any activity that occurs under your username. You are responsible for keeping your password secure.

#### Kibbut

*   **Your Kibbut Account.** If you create a Task or website on Kibbut, and you are fully responsible for all activities that occur in connection with the Task. You must immediately notify Kibbut of any unauthorized uses your account, or any other breaches of security. Kibbut will not be liable for any acts or omissions by you, including any damages of any kind incurred as a result of such acts or omissions.

*   **Responsibility of Users.** If you create a Task, you are entirely responsible for the content of, and any harm resulting from, that Task or your conduct. That is the case regardless of what form the Content takes, which includes, but is not limited to text, photo, video, audio, or code. By using Kibbut, you represent and warrant that your Content and conduct do not violate these Terms or the User Guidelines. By submitting a Task to Kibbut for inclusion on the platform, you grant Kibbut a world-wide, royalty-free, and non-exclusive license to reproduce, modify, adapt and publish the Content solely for the purpose of displaying, distributing, and promoting your Task. If you delete Content, Kibbut LLC will use reasonable efforts to remove it from Kibbut, but you acknowledge that caching or references to the Content may not be made immediately unavailable. Without limiting any of those representations or warranties, Kibbut has the right (though not the obligation) to, in Kibbut sole discretion, (i) refuse or remove any content that, in Kibbut's reasonable opinion, violates any Kibbut policy or is in any way harmful or objectionable, or (ii) terminate or deny access to and use of Kibbut to any individual or entity for any reason. Kibbut will have no obligation to provide a refund of any amounts previously paid.

*   **Advertisements.** Kibbut reserves the right to display advertisements on your Task.

*   **Prohibited Uses.** You may not use Kibbut for any unlawful purposes, or in furtherance of illegal activities. We may terminate your access to Kibbut Features if we determine (in our sole discretion) that your use is in any way harmful or objectionable.

*   **Third Party Services.** You may choose to set up and/or use third party services, such [Stripe](https://stripe.com/) or [PayPal](https://www.paypal.com/) to collect payment, [TaxJar](https://www.taxjar.com/) to calculate taxes, or [EasyPost](https://www.easypost.com/) to manage shipping. If you do so, be aware that some of your — and your customers’ — data may be passed to the respective third party, and the respective third party’s terms of service, privacy policy, and other policies may apply. We are not involved in these relationships. Please note that some of these third party services (like TaxJar) may be enabled by default, but you may disable them before your store is set up. If you do not want to use these third party services, please disable them.

*   **Tax Calculations.** You are responsible for all taxes and fees associated with your ecommerce activities. You must collect, report, and/or pay the correct amounts to the appropriate authorities, if applicable, and if needed, inform your customers about any taxes they may be required to pay and issue appropriate invoices. While some Ecommerce Features allow you to include sales taxes or Value Added Taxes in transactions, you should not rely solely on these features. We make our best efforts to keep our content and documents up-to-date, but because the tax law changes rapidly, we cannot guarantee that all the services are completely current. Tax laws differ from jurisdiction to jurisdiction and may be subject to different interpretations by different authorities. We recommend you consult an appropriate tax professional for your specific tax situation.**

*   **Your Responsibilities.** You are solely responsible for all of your ecommerce activities, this means that:
    *   We are not involved in your relationships or transactions with any customer or potential customer.
    *   You are responsible for resolving all support questions, comments, and complaints, including refunds, chargebacks, or pricing questions. You should provide contact information so that customers may contact you with questions or complaints.
    *   You are responsible for delivering items sold to your customers, and for fulfilling all promises, representations, or warranties you make to them in connection with a sale.


#### Responsibility of Visitors.

Kibbut has not reviewed, and cannot review, all of the material, including computer software, posted to our Services, and cannot therefore be responsible for that material's content, use or effects. By operating our Services, Kibbut does not represent or imply that it endorses the material there posted, or that it believes such material to be accurate, useful, or non-harmful. You are responsible for taking precautions as necessary to protect yourself and your computer systems from viruses, worms, Trojan horses, and other harmful or destructive content. Our Services may contain content that is offensive, indecent, or otherwise objectionable, as well as content containing technical inaccuracies, typographical mistakes, and other errors. Our Services may also contain material that violates the privacy or publicity rights, or infringes the intellectual property and other proprietary rights, of third parties, or the downloading, copying or use of which is subject to additional terms and conditions, stated or unstated. Kibbut disclaims any responsibility for any harm resulting from the use by visitors of our Services, or from any downloading by those visitors of content there posted.


#### Copyright Infringement and DMCA Policy.

As Kibbut asks others to respect its intellectual property rights, it respects the intellectual property rights of others. If you believe that material located on or linked to by Kibbut violates your copyright, you are encouraged to notify Kibbut. Kibbut will respond to all such notices, including as required or appropriate by removing the infringing material or disabling all links to the infringing material. Kibbut will terminate a visitor's access to and use of the website if, under appropriate circumstances, the visitor is determined to be a repeat infringer of the copyrights or other intellectual property rights of Kibbut or others. In the case of such termination, Kibbut will have no obligation to provide a refund of any amounts previously paid to Kibbut.

#### Intellectual Property.

This Agreement does not transfer from Kibbut to you any Kibbut or third party intellectual property, and all right, title, and interest in and to such property will remain (as between the parties) solely with Kibbut. Kibbut, and all other trademarks, service marks, graphics and logos used in connection with Kibbut or our Services, are trademarks or registered trademarks of Kibbut or Kibbut's licensors. Other trademarks, service marks, graphics and logos used in connection with our Services may be the trademarks of other third parties. Your use of our Services grants you no right or license to reproduce or otherwise use any Kibbut or third-party trademarks.


#### Changes.

We are constantly updating our Services, and that means sometimes we have to change the legal terms under which our Services are offered. If we make changes that are material, we will let you know by posting on one of our blogs, or by sending you an email or other communication before the changes take effect. The notice will designate a reasonable period of time after which the new terms will take effect. If you disagree with our changes, then you should stop using our Services within the designated notice period. Your continued use of our Services will be subject to the new terms. However, any dispute that arose before the changes shall be governed by the Terms (including the binding individual arbitration clause) that were in place when the dispute arose.

#### Termination.

Kibbut may terminate your access to all or any part of our Services at any time, with or without cause, with or without notice, effective immediately. If you wish to terminate this Agreement or your Kibbut account (if you have one), you may simply discontinue using our Services. All provisions of this Agreement which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.

#### Disclaimer of Warranties.

**Our Services are provided "as is." Kibbut and its suppliers and licensors hereby disclaim all warranties of any kind, express or implied, including, without limitation, the warranties of merchantability, fitness for a particular purpose and non-infringement. Neither Kibbut nor its suppliers and licensors, makes any warranty that our Services will be error free or that access thereto will be continuous or uninterrupted. You understand that you download from, or otherwise obtain content or services through, our Services at your own discretion and risk.**

#### Jurisdiction and Applicable Law.

**Except to the extent applicable law, if any, provides otherwise, this Agreement, any access to or use of our Services will be governed by the laws of the state of California, U.S.A., excluding its conflict of law provisions, and the proper venue for any disputes arising out of or relating to any of the same will be the state and federal courts located in San Francisco County, California.**

#### Arbitration Agreement.

**Except for claims for injunctive or equitable relief or claims regarding intellectual property rights (which may be brought in any competent court without the posting of a bond), any dispute arising under this Agreement shall be finally settled in accordance with the Comprehensive Arbitration Rules of the Judicial Arbitration and Mediation Service, Inc. ("JAMS") by three arbitrators appointed in accordance with such Rules. The arbitration shall take place in San Francisco, California, in the English language and the arbitral decision may be enforced in any court. The prevailing party in any action or proceeding to enforce this Agreement shall be entitled to costs and attorneys' fees.**

#### Limitation of Liability.

**In no event will Kibbut, or its suppliers or licensors, be liable with respect to any subject matter of this Agreement under any contract, negligence, strict liability or other legal or equitable theory for: (i) any special, incidental or consequential damages; (ii) the cost of procurement for substitute products or services; (iii) for interruption of use or loss or corruption of data; or (iv) for any amounts that exceed the fees paid by you to Kibbut under this Agreement during the twelve (12) month period prior to the cause of action. Kibbut shall have no liability for any failure or delay due to matters beyond their reasonable control. The foregoing shall not apply to the extent prohibited by applicable law.**

#### General Representation and Warranty.

You represent and warrant that your use of our Services:

*   Will be in strict accordance with this Agreement;
*   Will comply with all applicable laws and regulations (including without limitation all applicable laws regarding online conduct and acceptable content, the transmission of technical data exported from the United States or the country in which you reside, privacy, and data protection); and
*   Will not infringe or misappropriate the intellectual property rights of any third party.

#### US Economic Sanctions.

You expressly represent and warrant that your use of our Services and or associated services and products is not contrary to applicable U.S. Sanctions. Such use is prohibited, and Kibbut reserves the right to terminate accounts or access of those in the event of a breach of this condition.

#### Indemnification.

You agree to indemnify and hold harmless Kibbut, its contractors, and its licensors, and their respective directors, officers, employees, and agents from and against any and all claims and expenses, including attorneys' fees, arising out of your use of our Services, including but not limited to your violation of this Agreement.

#### Translation.

These Terms of Service were originally written in English (US). We may translate these terms into other languages. In the event of a conflict between a translated version of these Terms of Service and the English version, the English version will control.

#### Miscellaneous.

This Agreement constitutes the entire agreement between Kibbut and you concerning the subject matter hereof, and they may only be modified by a written amendment signed by an authorized executive of Kibbut, or by the posting by Kibbut of a revised version.

If any part of this Agreement is held invalid or unenforceable, that part will be construed to reflect the parties' original intent, and the remaining portions will remain in full force and effect. A waiver by either party of any term or condition of this Agreement or any breach thereof, in any one instance, will not waive such term or condition or any subsequent breach thereof.

You may assign your rights under this Agreement to any party that consents to, and agrees to be bound by, its terms and conditions; Kibbut may assign its rights under this Agreement without condition. This Agreement will be binding upon and will inure to the benefit of the parties, their successors and permitted assigns.
