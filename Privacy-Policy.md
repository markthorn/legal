Your privacy is critically important to us. At Kibbut, we have a few fundamental principles:

*   We are thoughtful about the personal information we ask you to provide and the personal information that we collect about you through the operation of our services.
*   We store personal information for only as long as we have a reason to keep it.
*   We help protect you from overreaching government demands for your personal information.
*   We aim for full transparency on how we gather, use, and share your personal information.

Below is our Privacy Policy, which incorporates and clarifies these principles.

### Information We Collect

We only collect information about you if we have a reason to do so–for example, to provide our Services, to communicate with you, or to make our Services better. We collect information in three ways: if and when you provide information to us, automatically through operating our Services, and from outside sources. Let’s go over the information that we collect.

#### _Information You Provide to Us_

It’s probably no surprise that we collect information that you provide to us. The amount and type of information depends on the context and how we use the information. Here are some examples:

*   **Basic Account Information:** We ask for basic information from you in order to set up your account. For example, we require individuals who sign up for a Kibbut account to provide a username and email address–and that’s it. You may provide us with more information–like your name–but we don’t require that information to create a Kibbut account.
*   **Public Profile Information:** If you have an account with us, we collect the information that you provide for your public profile. For example, if you have a Kibbut account, your username is part of that public profile, along with any other information you put into your public profile, such as a photo or an “About Me” description. Your public profile information is just that–public–so please keep that in mind when deciding what information you would like to include.
*   **Transaction and Billing Information:** If you buy something from us, you will provide additional personal and payment information that is required to process the transaction and your payment, such as your name, credit card information, and contact information.
*   **Ecommerce Site Information:** If you use our ecommerce Services to sell products or services to others, you will have to create a Kibbut account. You may also provide us with information about your financial account to set up a payments integration, such as the email address for your Stripe or PayPal account or your bank account information.
*   **Communications With Us (Hi There!):** You may also provide us information when you respond to surveys, or communicate with our Support team.

#### _Information We Collect Automatically_

We also collect some information automatically:

*   **Log Information:** Like most online service providers, we collect information that web browsers, mobile devices, and servers typically make available, such as the browser type, IP address, unique device identifiers, language preference, referring site, the date and time of access, operating system, and mobile network information. We collect log information when you use our Services.
*   **Usage Information:** We collect information about your usage of our Services. We also collect information about what happens when you use our Services, along with information about your device (e.g., screen size, name of cellular network, and mobile device manufacturer). We use this information to, for example, provide our Services to you, as well as get insights on how people use our Services, so we can make our Services better.
*   **Location Information:** We may determine the approximate location of your device. We collect and use this information to, for example, calculate how many people visit our Services from certain geographic regions. We may also collect information about your precise location via our mobile apps (when, for example, you post a photograph with location information) if you allow us to do so through your mobile device operating system’s permissions.
*   **Stored Information:** We may access information stored on your mobile device via our mobile app. We access this stored information through your device operating system’s permissions. For example, if you give us permission to access the photographs on your mobile device’s camera roll, our Services may access the photos stored on your device when you upload a picture of your lawn for someone to mow.

#### _Information We Collect from Other Sources_

We may also get information about you from other sources. For example, if you create or log into your WordPress.com account through another service (like Facebook), we will receive information from that service (such as your username, basic profile information, and friends list) via the authorization procedures used by that service. The information we receive depends on which services you authorize and any options that are available. We may also get information, such as a mailing address, from third party services about individuals who are not yet our users (…but we hope will be!), which we may use, for example, for marketing and advertising purposes like postcards and other mailers advertising our services.

### How And Why We Use Information

#### _Purposes for Using Information_

We use information about you as mentioned above and for the purposes listed below:

*   To provide our Services–for example, to set up and maintain your account,;
*   To further develop and improve our Services–for example by adding new features that we think our users will enjoy or help improve functionality;
*   To monitor and analyze trends and better understand how users interact with our Services, which helps us improve our Services and make them easier to use;
*   To measure, gauge, and improve the effectiveness of our advertising, and better understand user retention and attrition--for example, we may analyze how many individuals purchased something after receiving a marketing message or the features used by those who continue to use our Services after a certain length of time;
*   To monitor and prevent any problems with our Services, protect the security of our Services, detect and prevent fraudulent transactions and other illegal activities, fight spam, and protect the rights and property of Kibbut and others, which may result in us declining a transaction or the use of our Services;
*   To communicate with you, for example through an email, about offers and promotions offered by Kibbut and others we think will be of interest to you, solicit your feedback, or keep you up to date on Kibbut and our products; and
*   To personalize your experience using our Services, provide content recommendations, target our marketing messages to groups of our users, and serve relevant advertisements.

#### _Legal Bases for Collecting and Using Information_

A note here for those in the European Union about our legal grounds for processing information about you under EU data protection laws, which is that our use of your information is based on the grounds that: (1) The use is necessary in order to fulfill our commitments to you under our Terms of Service or other agreements with you or is necessary to administer your account (2) The use is necessary for compliance with a legal obligation; or (3) The use is necessary in order to protect your vital interests or those of another person; or (4) We have a legitimate interest in using your information--for example, to provide and update our Services, to improve our Services so that we can offer you an even better user experience, to safeguard our Services, to communicate with you, to measure, gauge, and improve the effectiveness of our advertising, and better understand user retention and attrition, to monitor and prevent any problems with our Services, and to personalize your experience; or (5) You have given us your consent

### Sharing Information

#### _How We Share Information_

We do not sell our users’ private personal information. We share information about you in the limited circumstances spelled out below and with appropriate safeguards on your privacy:

*   **Subsidiaries, Employees, and Independent Contractors:** We may disclose information about you to our subsidiaries, our employees, and individuals who are our independent contractors that need to know the information in order to help us provide our Services or to process the information on our behalf. We require our subsidiaries, employees, and independent contractors to follow this Privacy Policy for personal information that we share with them.
*   **Third Party Vendors:** We may share information about you with third party vendors who need to know information about you in order to provide their services to us, or to provide their services to you or your site. This group includes vendors that help us provide our Services to you (like payment providers that process your credit and debit card information, fraud prevention services that allow us to analyze fraudulent payment transactions, postal and email delivery services that help us stay in touch with you, customer chat and email support services that help us communicate with you), those that assist us with our marketing efforts (e.g. by providing tools for identifying a specific marketing target group or improving our marketing campaigns), those that help us understand and enhance our Services (like analytics providers).
*   **Legal Requests:** We may disclose information about you in response to a subpoena, court order, or other governmental request.
*   **To Protect Rights, Property, and Others:** We may disclose information about you when we believe in good faith that disclosure is reasonably necessary to protect the property or rights of Kibbut, third parties, or the public at large. For example, if we have a good faith belief that there is an imminent danger of death or serious physical injury, we may disclose information related to the emergency without delay.
*   **Business Transfers:** In connection with any merger, sale of company assets, or acquisition of all or a portion of our business by another company, or in the event that Kibbut goes out of business or enters bankruptcy, user information would likely be one of the assets that is transferred or acquired by a third party. If any of these events were to happen, this Privacy Policy would continue to apply to your information and the party receiving your information may continue to use your information, but only consistent with this Privacy Policy.
*   **With Your Consent:** We may share and disclose information with your consent or at your direction. For example, we may share your information with third parties with which you authorize us to do so, such as the social media services that you connect to.
*   **Aggregated or De-Identified Information:** We may share information that has been aggregated or reasonably de-identified, so that the information could not reasonably be used to identify you.

### Security

While no online service is 100% secure, we work very hard to protect information about you against unauthorized access, use, alteration, or destruction, and take reasonable measures to do so, such as monitoring our Services for potential vulnerabilities and attacks.

### Choices

You have several choices available when it comes to information about you:

*   **Limit the Information that You Provide:** If you have an account with us, you can choose not to provide the optional account information. Please keep in mind that if you do not provide this information, certain features of our Services may not be accessible.
*   **Limit Access to Information On Your Mobile Device:** Your mobile device operating system should provide you with the ability to discontinue our ability to collect stored information or location information via our mobile apps. If you do so, you may not be able to use certain features.
*   **Opt-Out of Electronic Communications:** You may opt out of receiving promotional messages from us. Just follow the instructions in those messages. If you opt out of promotional messages, we may still send you other messages, like those about your account and legal notices.

### Your Rights

If you are located in certain countries, including those that fall under the scope of the European General Data Protection Regulation (AKA the “GDPR”), data protection laws give you rights with respect to your personal data, subject to any exemptions provided by the law, including the rights to:

*   Request access to your personal data;
*   Request correction or deletion of your personal data;
*   Object to our use and processing of your personal data;
*   Request that we limit our use and processing of your personal data; and
*   Request portability of your personal data.

You can usually access, correct, or delete your personal data using your account settings and tools that we offer, but if you aren’t able to do that, or you would like to contact us about one of the other rights, scroll down to How to Reach Us to, well, find out how to reach us. EU individuals also have the right to make a complaint to a government supervisory authority.

### How to Reach Us

If you have a question about this Privacy Policy, or you would like to contact us about any of the rights mentioned in the Your Rights section above, please [contact us](mailto:legal@kibbut.com).

### Other Things You Should Know (Keep Reading!)

#### _Ads and Analytics Services Provided by Others_

Ads appearing on any of our Services may be delivered by advertising networks. Other parties may also provide analytics services via our Services. These ad networks and analytics providers may set tracking technologies (like cookies) to collect information about your use of our Services. These technologies allow these third parties to recognize your device to compile information about you or others who use your device. This information allows us and other companies to, among other things, analyze and track usage, determine the popularity of certain content, and deliver advertisements that may be more targeted to your interests. Please note this Privacy Policy only covers the collection of information by Kibbut and does not cover the collection of information by any third party advertisers or analytics providers.

### Privacy Policy Changes

Although most changes are likely to be minor, Kibbut may change its Privacy Policy from time to time. Kibbut encourages visitors to frequently check this page for any changes to its Privacy Policy. Your further use of the Services after a change to our Privacy Policy will be subject to the updated policy.

That’s it! Thanks for reading.
