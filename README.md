# Kibbut Legal

### What is this?
This is the source of several "legal documents" on Kibbut, including the Terms of Service.

### License
As noted in our license file, the content here is licensed under the [Creative Commons Attribution-ShareAlike 4.0 International license](http://creativecommons.org/licenses/by-sa/4.0/). Please feel free to make alterations via pull-request.